# i2_Submission

Interview 2 Submission

This repository is created to answer the questions posed by the take home assignment from interview 2.

Requirements.txt indicates the packages required. 

Questions are respectively answered in:

1) q1_anagram.py 
2) q2.email_addresses.py
3) q3_buying_on_budget.py

Some tests cases for each are available at test_q1.py, test_q2.py and test_q3.py. 