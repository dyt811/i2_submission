#!/usr/bin/python
# coding=utf-8

# Input: string

# Output: number of pairs of distinct substrings anagram of each other; two strings are anagrams of each
# other if and only if they consist of exactly the same letters (repetition counts): “more” and “rome” are
# anagrams; “add” and “ad” are not.

# Assumptions:
# String/data list input is already sanitized and does not pose security risk, exploits etc.

from collections import Counter

def anagram_solver(string_input: str) -> int:
    """
    This function analyze the string, break them down into possible anagram pairs and return all of them.
    Attempt 1: build the dumbest iterators and have the worst performance and least efficient solution, but solve the problem.
    :param string_input: the input string which is to be scanned
    :return: integer representing the total number of such pairs
    """
    list_anagrams = []

    # Substring can start with length 1 all the way to len(string_input)
    for len_substring in range(1,len(string_input)):

        # e.g. inputstring = abcdefg, len_substring = 2, result in: ab, bc, cd, de, ef, fg total of 5 possible positions = 7 -2
        index_substring_last = len(string_input) - len_substring

        for index_source in range(index_substring_last):

            # Define the source of comparisons
            substring_source = string_input[index_source:index_source+len_substring]

            # Define the moving destination of comparisons
            # For each substring, can iterate through up until the position of last possible substring.
            for index_destination in range(index_source+1, index_substring_last+1): # index_substring_last needs + 1 because that index needs to be included (inclusive).
                # move from index_source + 1 to check the substring all the way to the end.
                substring_destination = string_input[index_destination:index_destination + len_substring]

                # Counter is a python function that tabulate the occurrence distribution
                if Counter(substring_source) == Counter(substring_destination):
                    # if the Counter is identical, they are anagram of each other.
                    list_anagrams.append((substring_source, substring_destination))
    return len(list_anagrams)