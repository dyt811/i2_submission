#!/usr/bin/python
# coding=utf-8

from typing import List


# Input: string block

# Output: list of emails.

# Assumptions:

# Username portion can have more than one period.
# Username in text is preceded by a SPACE.
# Domain portion of the email address can have multiple component: e.g. @asdf.com.cn
# Domain portion ends with a space may proceeded by a punctuation mark (!.,; etc)
# String/data list input is already sanitized and does not pose security risk, exploits etc.

import re

def email_address_solver(string_block: str) -> List[str]:
    """
    This function parse the large string chunk and return all UNIQUE emails in a lexicographical order.
    :param string_block: a block of string which may or may not contain email with some nearby punctuations
    :return: list_emails_returned: a list of emails cleaned up and sorted alphabetically.
    """

    # Regular Expression Pattern to look for should be something like:
    # Space .* @ .* Space
    pattern_email = r"\s[^\s]*@[^\s]*\s"
    pattern_bad_ending = r"\W\s" # in cases where it ends like ".com. " where ". " needs to be trimmed. Using \W for anything no-alphanumerical.

    re_email = re.compile(pattern_email)

    # return all instances of the raw string however, they are padded by space and may have punctuation at the end

    list_raw_emails = re_email.findall(string_block)

    # Instantiate the list to be returned.
    list_emails_returned = []

    # loop through the entire list of regular expression results, further fine tune them.

    for email in list_raw_emails:
        # remove bad ending:
        string_email_ending_trimmed = re.sub(pattern_bad_ending, '', email)
        # remove space
        string_email_proper = string_email_ending_trimmed.strip()
        # append to the return list.
        list_emails_returned.append(string_email_proper)

    # Sort the list alphabetically.
    list_emails_returned.sort()

    # Return sorted list.
    return list_emails_returned

