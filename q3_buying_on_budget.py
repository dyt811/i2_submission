#!/usr/bin/python
# coding=utf-8

import numpy as np
from typing import Tuple, List, Set
import collections
import sys
from copy import deepcopy
from random import randint
import logging

# Key assumptions:

# At least one item in each of N categories.
# Batch selling.
# Assuming that all seller lists does not contain any other objects.
# Assuming each seller have at least one item to sell.
# Assuming each seller would only have 1 or 0 in M list, no other values.
# Since this is an NP complete problem, I choose to focus on most likely scenerios and built most around pairs of merchants satisfying the criteria than N combination of merchants.
# String/data list input is already sanitized and does not pose security risk, exploits etc.


# Logger for easier debugging
logger = logging.getLogger()
logging.basicConfig(level=logging.DEBUG)

# Merchant named tuple to make thigns a bit easier to access and read.
Merchant = collections.namedtuple('Merchant', 'price merchandise')

class MerchantSelection():
    """
    This is a classical well known NP-complete problem under the name Set Cover Problem: https://en.wikipedia.org/wiki/Set_cover_problem

    # Here is a best attempt to approximate the best solution using known information.

    # There are FOUR costs been sought out, and best results among the four are returned.

    # 1. First Collection: when sorted all merchants by cost from small to large, the first combination of merchants that meet the criteria. (Guarntee at least a solution)

    # 2. Largest MerchantS Approaches: after iterating through all the merchants, find the merchants that sell the most items, then and iterate through them and try to find any other merchants that has the matching set (matching set not guarnteed).

    # 3. Single Merchant: after iterating through all the merchant, we should identify any merchants that sells ALL (matching set not guarnteed).

    # 4. Cheapest Merchant: while looping through, try to see if there is any combination that matches the cheapest merchant (matching set not guarnteed).

    # This is not a complete solution as there is no known ideal solutions to deterministically solve NP-complete problems.
    """


    def __init__(self, M, P):

        # Store input variables. for merchant and price.
        self.list_availabilities_of_merchants = M
        logger.info(M)

        self.list_price_of_merchant = P
        logger.info(P)

        # Join them via zipping for easier sorting.
        self.list_price_availability: List[Tuple[int, list]] = list(zip(self.list_price_of_merchant, self.list_availabilities_of_merchants))

        # Final list of list in increasing PRICE order.
        self.list_price_availability.sort()

        # N of Merchandise
        self.merchandise = len(self.list_availabilities_of_merchants[0])

        # e.g. start with [1, 2, 3, 4, 5, 6, 7, ... n], len = n
        # This numpy array represent the full array of items. It will facilitate COUNTER/SET operation later on.
        self.array_merchandise = np.array(range(1, self.merchandise + 1))

        # {1, 2, 3, 4, 5, 6... n }, we are looking for every single item within this set.
        self.set_merchandise_longterm = set(range(1, self.merchandise + 1))

        # short term goal of the objects to look for.
        self.set_merchandise_lookingfor = self.set_merchandise_longterm

        # Keep track of relevant merchants from the first pass.
        self.list_relevant_merchants: List[Merchant] = []

        #=========================
        # Largest MerchantS Section
        # Keep track of the merchant with most merchandise

        self.list_largest_merchants: List[Merchant] = []

        # =========================
        # First Merchant Section
        self.first_merchant = Merchant(
            price=self.list_price_of_merchant[0],
            merchandise=self.list_availabilities_of_merchants[0])
        self.set_merchandise_lookingfor_first_merchant: set = self.determine_matching_set(
            set(self.get_set_avalability(self.first_merchant.merchandise)), self.set_merchandise_longterm
        )

        # =========================
        # This is only updated once a combination has been found, for least effort search. All value instantiated with highest number possible.
        self.cost_first_combo: float = float("inf")
        self.cost_single_merchant: float = float("inf")
        self.list_cost_largest_merchant: List[int] = [sys.maxsize]
        self.cost_cheapest_merchant: float = float("inf")

        assert len(self.list_availabilities_of_merchants) == len(self.list_price_of_merchant)   # the number of seller who offer availabilities should always have price associated with it.

        # =========================
        # Do the first pass of the data.
        # First step will:
        # - find the first combination of merchants that will yield the full combination set, clean it, set the min_costv1 to that.
        # _ find any match for first merchant (cheapest merchant in sorted).
        # - find the merchants that sell the most items (hence easiest to find cheapest match, in subsequent path)
        self.first_pass()

        # =========================
        # Do the second pass of the data.
        # - find the merchant sells the matching set to the exist list of merchants that sell the most.
        self.second_pass()

        # End of Best effort, time to see what was found.

        # Update the best cost
        self.best_cost = min(
            [self.cost_first_combo,
             self.cost_cheapest_merchant,
             self.cost_single_merchant,
             min(self.list_cost_largest_merchant)],
        )
        logger.info(f"Cost of fist combo: {self.cost_first_combo}")
        logger.info(f"Cost from Largest Merchants: {min(self.list_cost_largest_merchant)}")
        logger.info(f"Cost from Cheapest Merchant: {self.cost_cheapest_merchant}")
        logger.info(f"Cost of Single Merchant: {self.cost_single_merchant}")
        logger.info(f"Best: {self.best_cost}")

    def first_pass(self):
        """
        # Do the first pass of the data.
        # First step will find the first combination of merchants that will yield the full combination set
        # It will find any merchant complementing perfectly with the first merchant.
        # It will identify a list of all merchants that sell the most number of items.
        """
        self.first_merchant_match_found = False
        self.full_merchant_match_found = False

        # Traverse the merchants from cheap to high.
        for index, (price, list_availability) in enumerate(self.list_price_availability):

            # Keep track of the merchant with the largest availabilities for secondary searches.
            self.update_largest_merchant(index)

            # Get the set availability or this particular merchant.
            set_availability = self.get_set_avalability(list_availability)

            # Record the first merchant which has all the price (cheapest), and no longer look further (otherwise, the price gets maximized instead of minimized)
            if set_availability.issuperset(self.set_merchandise_longterm) and not self.full_merchant_match_found:
                self.cost_single_merchant = price
                self.full_merchant_match_found = True
            # found who can match perfectly to the first merchant, and no longer look further (otherwise, the price gets maximized instead of minimized)
            if set_availability.issuperset(self.set_merchandise_lookingfor_first_merchant) and not self.first_merchant_match_found:
                self.cost_cheapest_merchant = self.first_merchant.price + price
                self.first_merchant_match_found = True

            # Compute the intersection of the two sets.
            set_merchant_overlap_needs = set_availability.intersection(self.set_merchandise_lookingfor)

            # if merchant has at least one item we need, then update the set_merchandise_lookingfor criteria
            if len(set_merchant_overlap_needs) >= 1:

                # Update goal set since now we found some of the items we are looking for by removing elements in intersections that were not in the goal.
                # e.g. [1, 7, ....]
                self.set_merchandise_lookingfor = self.set_merchandise_lookingfor - set_merchant_overlap_needs

                # store this list for further review.
                self.list_relevant_merchants.append(Merchant(price=price, merchandise=set_availability))

                # Trim redundant merchant while the list is small to avoid performance penalities.
                self.remove_redundant_merchants()

                # This will only be done once since the previous if conditions will fail in subsequent attempts
                if len(self.set_merchandise_lookingfor) == 0:
                    # found all merchandise
                    # Compute the maximum price.
                    self.cost_first_combo = self.calculate_cost_relevant_merchants()

    def second_pass(self):
        """
        Second_pass to find a best pair match to the largest merchant, hoping that this would be one of the lowest price.
        :return:
        """
        # Assuming at this point, we already know the largest merchant.
        assert self.list_largest_merchants != []
        logger.info(f"Largest Merchant has: {self.get_largest_merchant_inventory()} items")

        # Check every single of the list of largest merchant
        for largest_merchant in self.list_largest_merchants:

            # Now that we know the largest merchant, time to see what they are looking for:
            set_lookingfor = self.determine_matching_set(
                self.get_set_avalability(largest_merchant.merchandise),
                self.set_merchandise_longterm
            )

            # Traverse the merchants from cheap to high.
            for index, (price, list_availability) in enumerate(self.list_price_availability):

                set_availability = self.get_set_avalability(list_availability)

                # Perfect ONE merchant for all the needs.
                if set_availability.issuperset(set_lookingfor):
                    self.list_cost_largest_merchant.append(largest_merchant.price + price)
                    return
        logger.info(self.list_cost_largest_merchant)

    def get_set_avalability(self, list_availability):
        """
        Retun the set availability from a list.
        :param list_availability:
        :return:
        """
        # e.g. [0, 1, 1, 0, 1, 1 ... ], len = n
        # Iterate this list from small price to higher price.
        array_availability = np.array(list_availability)

        # Numpy element wise multiplications e.g. [0, 2, 3, 0, 5, 6 ... ] len = n
        array_encoded_availabitlies = self.array_merchandise * array_availability

        # e.g. [0, 2, 3, 5, 6... ]
        set_availability = set(array_encoded_availabitlies)

        return set_availability

    def update_largest_merchant(self, index):
        """
        Update the list of largest merchants that sells the most product and how much it cost.
        :param index:
        :return:
        """
        (price, list_availability) = self.list_price_availability[index]

        len_merchant_availability = len(self.get_set_avalability(list_availability))

        largest_set_N = self.get_largest_merchant_inventory()

        # Only update if the availability increase, if the same, ignore as the list is sorted small to largest.
        if len_merchant_availability > largest_set_N:
            # Clean slate, since now the largest N has been found.
            self.list_largest_merchants.clear()

            # Append this as the first merchant found that sell that many goodies.
            self.list_largest_merchants.append(
                Merchant(
                    price=price,
                    merchandise=list_availability
                )
            )
        elif len_merchant_availability == largest_set_N:
            # Append this as the it is not the first merchant found that sell that many goodies.
            self.list_largest_merchants.append(
                Merchant(
                    price=price,
                    merchandise=list_availability
                )
            )

    def get_largest_merchant_inventory(self) -> int:
        """
        Get the number of items sold by the largest known merchants.
        :return:
        """
        if len(self.list_largest_merchants) == 0:
            return 0
        else:
            # since all merchants in this list has the same number of items, just use the first one as an indicator of total number of merchandise has for sell.
            return self.get_non_zero_length(self.list_largest_merchants[0].merchandise)

    def get_non_zero_length(self, list_merchandise_available: list) -> int:
        """
        Given a list, convert it to set, return the count of non-zero elements from it.
        Used primarily to help calculate which merchant amongst  fo the list of the merchant has the largest collection
        :param list_merchandise_available:
        :return:
        """
        inventory = self.get_set_avalability(list_merchandise_available)

        # Remove the fact that the set might contain "0" for null set. e.g. {0, 1, 2, 3}
        if inventory.issuperset({0}):
            return len(inventory) - 1
        else:
            return len(inventory)


    def remove_redundant_merchants(self):
        """
        This is used to further fine tune cost by minimizing redundancies among the selected merchants that make up for the first combination
        """

        list_index_removal = []

        # Check all merchants,
        # Check from the smallest to the largest merchant set we have, to screen out any redundant subset merchant.
        for index, merchant in enumerate(self.list_relevant_merchants):

            # If second last, merchant, break.
            if index >= len(self.list_relevant_merchants) - 1:
                break

            # the merchnat in comparison with is the next merchant with slightly higher price
            for index_comparison in range(index+1, len(self.list_relevant_merchants)):
                # Do a subset check.
                if merchant.merchandise.issubset(self.list_relevant_merchants[index_comparison].merchandise):

                    # if the cheaper merchant is a subset of the more expensive merchant, log the index to be removed.
                    list_index_removal.append(index)
                    break

        # index deletion needs to happen from large to small.
        list_index_removal.sort(reverse=True)

        # Delete all redundant merchants.
        for index in list_index_removal:
            del self.list_relevant_merchants[index]

    def determine_matching_set(self, set_input: set, set_goal: set) -> Set:
        """
        Simpley return the matching set required to get all the objects.
        :param set_input:
        :param set_goal:
        :return:
        """
        return set_goal - set_input

    def calculate_cost_relevant_merchants(self):
        """
        This is used to calculate the cost from the list of relevant merchants.
        :param list_price_availability:
        :return:
        """
        price_total = 0
        for merchant in self.list_relevant_merchants:
            price_total = price_total + merchant.price
        return price_total


def randomGen(sorted=False):
    """
    Random Case Generator that generate multiple complicated scenerios abiding by the rules:
    1) merchant must sell SOMETHING
    2) combined, all merchants must sell enough.
    :return:
    """
    n = randint(4, 10)  # number of merchandise, to be within that range for easier visual inspection.
    m = randint(5, 8)  # number of merchants possible.

    # [1, 2, 3, 4 ... n ]
    items = range(1, n + 1) # items are named 1, to n+1 for a total of N objects. This is important as 0 indicates lack of availabilities later on and both array needs to be multiplied to each other to indicate availabilities l
    np_items = np.array(items)

    M = []
    P = []

    set_final = set()
    complete = False

    # Complete flag ensures the generator only exit the while loop when it has a feasible solution (i.e. has all items across all merchants)
    while not complete:

        # Clear previously used list.
        M.clear()
        P.clear()
        set_final.clear()

        complete = True

        # Loop for m Merchant (0 index based)
        for merchant in range(m):

            objects_availabitiles = []

            # Loop for n merchandise (0 index based)
            for object in range(n):
                objects_availabitiles.append(randint(0, 1))

            # Compute the unique items the merchant has.
            set_merchant_availabilities = set(np_items * objects_availabitiles)

            # If only one, (which is only possible in case of [0, 0, 0, 0, 0...0], not really a merchant, redraw.
            if len(set_merchant_availabilities) == 1: # When the set only contain ZEROes.
                complete = False

            # Calculate the final set combined across ALL merchants.
            set_final = set_final.union(set_merchant_availabilities)

            M.append(objects_availabitiles)
            P.append(randint(1, 100))

        # The final idea set should contain {1, 2, 3, ... n} and {0} because across ALL merchant, they should have all items and at least ONE merchant is missing an item (0).
        if len(set_final) < n + 1: # +1 because ZERO is in there.
            complete = False

    if sorted:
        together = list(zip(P, M))
        together.sort()
        P, M = zip(*together)

    return M, P

if __name__=="__main__":
    """
    M = [
        [0, 0, 1, 1],  # seller 1: buy, then discard.
        [0, 0, 0, 1],  # seller 2
        [1, 0, 0, 1],  # seller 3: buy, then discard
        [0, 0, 1, 1],  # seller 4: buy
        [1, 1, 0, 1],  # seller 5
        [1, 1, 1, 0],  # seller 6: buy, then keep searching until 4+6 for better deals.
        [1, 0, 1, 1],  # seller 7: BEST. Return.
        [1, 1, 0, 1]]  # seller 8

    P = [1,  # seller 1 price for the entire set
         2,  # seller 2 price for the entire set
         3,
         4,
         5,
         6,
         7,
         8,
         ]  # seller 3 price for the entire set.
    """
    M, P = randomGen() # Randomly generate M and P fitting the criteria
    test1 = MerchantSelection(M, P)
    print(test1.best_cost)