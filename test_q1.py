from q1_anagram import anagram_solver
import unittest
from string import ascii_letters

class TestAnagrams(unittest.TestCase):
    """
    Test cases for Anagram Solvers
    """
    def test_single_input(self):
        """
        Check random single character
        :return:
        """
        for character in ascii_letters:
            self.assertEqual(anagram_solver(character), 0)

    def test_adda(self):
        """
        Check the given adda example.
        # adda
        # a&a, d&d, ad&da, add&dda
        :return:
        """
        self.assertEqual(anagram_solver("adda"), 4)

    def test_addad(self):
        """
        Check the additional addad example.
        # addad
        # a&a, d&d, d&d, d&d, ad&da, ad&ad, da&ad, add&dda, add&dad, dda&dad

        :return:
        """
        self.assertEqual(anagram_solver("addad"), 10)

    def test_abcd(self):
        """
        Check the abcd null example.
        :return:
        """
        self.assertEqual(anagram_solver("abcd"), 0)

    def test_digits(self):
        """
        Check the given adda example.
        :return:
        """
        self.assertEqual(anagram_solver("12321"), 6)

    def test_repetitive_elements(self):
        """
        Check when lots of repeats
        :return:
        """
        self.assertEqual(anagram_solver("888888"), 35)




