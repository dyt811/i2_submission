#!/usr/bin/python
# coding=utf-8

from q2_email_addresses import email_address_solver
import unittest
from string import ascii_letters

class TestEmailAddress(unittest.TestCase):
    """
    Test cases for Anagram Solvers
    """
    def test_example_case(self):
        """
        Test using provided example.
        :return:
        """
        string_block = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu malesuada quam info@jumio.com. Proin id egestas ligula. Nullam volutpat eget odio quis feugiat. Suspedisse sed posuere ligula. Ut pulvinar nibh eros, nec pretium odio lacinia in. Maecenas elementum massa cicero-senator@jumio.com posuere accumsan. Integer tellus enim, aliquam vitae eros eget, tristique tempor tellus. Aenean sed laoreet justo. Fusce pellentesque ultrices tortor, vel rutrum leo venenatis ac. Suspendisse eros nibh, laoreet condimentum augue id, imperdiet faucibus sapien. Duis in cursus sapien, in sollicitudin justo. Integer tincidunt ac leo sed pulvinar."
        output = email_address_solver(string_block)
        self.assertEqual(len(output), 2)
        self.assertEqual(output, ['cicero-senator@jumio.com', 'info@jumio.com'])

    def test_empty_case(self):
        """
        Test when the input is empty string
        :return:
        """
        string_block = ""
        output = email_address_solver(string_block)
        self.assertEqual(output, [])
