from q3_buying_on_budget import MerchantSelection

import unittest

class TestMerchantSelection(unittest.TestCase):

    def test_dataset1(self):
        M = [
            [0, 0, 0, 1],  # seller 1: choosen, then discard.
            [0, 0, 0, 1],  # seller 2: ignored
            [1, 0, 0, 1],  # seller 3: choosen, then discard
            [1, 0, 1, 1],  # seller 4: choosen, then buy
            [1, 0, 0, 1],  # seller 5: ignore
            [1, 1, 0, 1],  # seller 6: choosen, then buy
            [1, 1, 1, 1],  # seller 7: SOLUTION.
            [1, 0, 0, 1]]  # seller 8:

        P = [1,  # seller 1 price for the entire set
             2,  # seller 2 price for the entire set
             3,
             4,
             5,
             6,
             7,
             8,
             ]  # seller 3 price for the entire set.
        test = MerchantSelection(M, P)
        self.assertEqual(test.best_cost, 7)

    def test_dataset2(self):
        M = [
            [0, 0, 0, 1],  # seller 1: buy, then discard.
            [0, 0, 0, 1],  # seller 2
            [1, 0, 0, 1],  # seller 3: buy, then discard
            [1, 0, 1, 1],  # seller 4: buy
            [1, 0, 0, 1],  # seller 5
            [1, 1, 0, 1],  # seller 6: buy, then keep searching until 4+6 for better deals.
            [1, 1, 1, 1],  # seller 7: BEST. Return.
            [1, 0, 0, 1]]  # seller 8

        P = [1,  # seller 1 price for the entire set
             2,  # seller 2 price for the entire set
             3,
             4,
             5,
             6,
             11,
             8,
             ]  # seller 3 price for the entire set.
        test = MerchantSelection(M, P)
        self.assertEqual(test.best_cost, 10)


    def test_dataset3(self):
        M = [
            [0, 0, 0, 1],  # seller 1: relevant, then discard.
            [0, 0, 1, 1],  # seller 2: relevant, then discard.
            [0, 1, 1, 1],  # seller 3: relevant, then buy
            [1, 0, 1, 1],  # seller 4: buy
            [1, 0, 0, 1],  # seller 5
            [1, 1, 0, 1],  # seller 6:
            [1, 1, 1, 1],  # seller 7: also best choice.
            [1, 0, 0, 1]]  # seller 8

        P = [1,  # seller 1 price for the entire set
             2,  # seller 2 price for the entire set
             3,
             4,
             5,
             6,
             7,
             8,
             ]  # seller 3 price for the entire set.
        test = MerchantSelection(M, P)
        self.assertEqual(test.best_cost, 7)

    def test_dataset4(self):
        M = [
            [0, 0, 0, 1],  # seller 1: buy, then discard.
            [0, 0, 1, 0],  # seller 2: buy, keep.
            [0, 1, 0, 0],  # seller 3: buy, then discard
            [0, 0, 1, 1],  # seller 4: ignore
            [0, 0, 0, 1],  # seller 5: ignore
            [0, 1, 0, 1],  # seller 6: ignore
            [0, 1, 1, 1],  # seller 7: ignore.
            [1, 1, 0, 1]]  # seller 8: buy.

        P = [1,  # seller 1 price for the entire set
             2,  # seller 2 price for the entire set
             3,
             4,
             5,
             6,
             7,
             8,
             ]  # seller 3 price for the entire set.
        test = MerchantSelection(M, P)
        self.assertEqual(test.best_cost, 10)


    def test_dataset5(self):
        M = [
            [0, 0, 1, 1],  # seller 1: buy, then discard.
            [0, 0, 1, 0],  # seller 2: ignore
            [0, 1, 0, 0],  # seller 3: buy
            [0, 1, 1, 0],  # seller 4: ignore
            [1, 0, 0, 1],  # seller 5: buy: define min_cost.
            [0, 1, 0, 1],  # seller 6:
            [0, 1, 1, 1],  # seller 7:
            [1, 1, 1, 1]]  # seller 8:

        P = [1,  # seller 1 price for the entire set
             1,  # seller 2 price for the entire set
             1,
             1,
             1,
             1,
             1,
             1,
             ]  # seller 3 price for the entire set.
        test = MerchantSelection(M, P)
        self.assertEqual(test.best_cost, 1)

    def test_case5(self):
        M = [[1, 1, 0, 1, 0, 1, 0, 0, 1, 0],
             [0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
             [0, 0, 1, 0, 1, 0, 0, 1, 1, 0],
             [0, 1, 0, 0, 0, 0, 0, 1, 1, 0],
             [1, 0, 1, 0, 0, 0, 1, 1, 1, 1],
             [0, 1, 0, 0, 1, 1, 0, 1, 1, 0],
             [1, 0, 0, 1, 1, 1, 0, 0, 1, 1],
             [1, 0, 1, 1, 0, 1, 1, 0, 1, 1]]
        P =  [60,
              39,
              62,
              30,
              90,
              8,
              72,
              13]
        test = MerchantSelection(M, P)
        self.assertEqual(test.best_cost, 21)

    def test_case6(self):
        M = ([1, 1, 0, 0, 0, 1, 0, 1, 1, 0],
             [0, 1, 1, 0, 1, 0, 1, 1, 0, 1],
             [1, 1, 0, 1, 1, 0, 1, 1, 1, 0],
             [1, 1, 1, 0, 0, 0, 1, 0, 0, 0],
             [1, 0, 0, 0, 0, 1, 0, 0, 1, 1])
        P = (15,
             23,
             36,
             36,
             81)
        test = MerchantSelection(M, P)
        self.assertEqual(test.best_cost, 74)





if __name__=="__main__":

    """
    M, P = TestMerchantSelection.randomGen(True)
    print(M)
    print(P)
    test = MerchantSelection(M, P)
    print(test.best_cost)
    """